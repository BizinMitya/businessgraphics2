package chart;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;
import kd.KDPoint;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

//класс для рисования круговой диаграммы
public class PieChart {
    private List<Color> colors;//список цветов для каждого сектора
    private List<KDPoint> kdPoints;//список точек KDPoint(название-значение)
    private Canvas canvas;//холст для рисования

    //конструктор
    public PieChart(List<Color> colors, List<KDPoint> kdPoints, Canvas canvas) {
        this.canvas = canvas;
        this.colors = colors;
        if (colors.size() == 0) {//если цветов 0,то в цикле заполняем случайными цветами список(каждый цвет - для своего сектора,поэтому их столько же,сколько точек)
            for (int i = 0; i < kdPoints.size(); i++) {
                colors.add(new Color(Math.random(), Math.random(), Math.random(), 1));//3 случайных цвета(красный,зелёный,синий и прозрачность 1)
            }
        }
        if (colors.size() < kdPoints.size()) {//если цветов меньше,чем точек,то аналогично заполняем оставшиеся цвета случайными
            for (int i = 0; i < kdPoints.size() - colors.size(); i++) {
                colors.add(new Color(Math.random(), Math.random(), Math.random(), 1));
            }
        }
        this.kdPoints = kdPoints;
    }

    //метод рисования круговой диаграммы
    public void drawChart() {
        if (kdPoints.size() > 0) {//если точек больше 0,то рисуем,иначе ничего не делаем
            double width = canvas.getWidth();//получаем текущую ширину холста(холст связан с размерами панели,на которой он находится,а паенль связана с размерами окна)
            double height = canvas.getHeight();//получаем текущую высоту
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
            Optional<KDPoint> sumPoint = kdPoints.stream().reduce((KDPoint, KDPoint2) ->
                    new KDPoint("sum", String.valueOf(Double.parseDouble(KDPoint.getValue()) + Double.parseDouble(KDPoint2.getValue()))));//делаем точку со значением - сумма всех других точек
            double sum = Double.parseDouble(sumPoint.get().getValue());//находим сумму всех значений точек
            double tempAngle = 0;//текущий угол 0
            double angle = 0;//текущий угол поворота 0
            double diameter = 0.8 * Math.min(width, height);//диаметр выбираем как 0.8 от минимума ширины и высоты
            double x0 = width / 2 - diameter / 2;//центр круга,x
            double y0 = height / 2 - diameter / 2;//центр круга,y
            graphicsContext.setFont(new Font("Segoe Print", 14));//ставим шрифт и его размер
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);//устаналиваем сглаживание для шрифтов
            //в цикле закрашиваем сектора
            for (int i = 0; i < kdPoints.size(); i++) {
                angle = 360 * Double.parseDouble(kdPoints.get(i).getValue()) / sum;//текущий угол поворота в градусах
                if (angle == angle) {//проверка на NaN числа angle(будет при нулевых значениях)
                    graphicsContext.setFill(colors.get(i));//ставим цвет заливки на нужный изсписка
                    graphicsContext.fillArc(x0, y0, diameter, diameter, tempAngle, angle, ArcType.ROUND);//заполняем текущий сектор
                    graphicsContext.setFill(Color.BLACK);//ставим заливку на чёрный(для букв)
                    graphicsContext.setStroke(Color.BLACK);//ставим цвет линии на чёрный(для линий подписей)
                    graphicsContext.strokeLine(width / 2 + 0.7 * Math.min(width, height) * Math.cos(Math.toRadians(tempAngle + angle / 2)) / 2,
                            height / 2 - 0.7 * Math.min(width, height) * Math.sin(Math.toRadians(tempAngle + angle / 2)) / 2,
                            width / 2 + 0.9 * Math.min(width, height) * Math.cos(Math.toRadians(tempAngle + angle / 2)) / 2,
                            height / 2 - 0.9 * Math.min(width, height) * Math.sin(Math.toRadians(tempAngle + angle / 2)) / 2);//проводим линию от сектора наружу(от 0.7 радиуса до 0.9 радиуса)
                    graphicsContext.fillText(String.valueOf((new BigDecimal(5 * angle / 18).setScale(2, RoundingMode.HALF_DOWN)).doubleValue()) + "%",
                            width / 2 + 0.95 * Math.min(width, height) * Math.cos(Math.toRadians(tempAngle + angle / 2)) / 2,
                            height / 2 - 0.95 * Math.min(width, height) * Math.sin(Math.toRadians(tempAngle + angle / 2)) / 2);//подписываем сектор
                    tempAngle += 360 * Double.parseDouble(kdPoints.get(i).getValue()) / sum;//увеличиваем текущий угол
                }
            }
        }
    }

    //метод рисования легенды
    public void drawLegend() {
        if (kdPoints.size() > 0) {//если точек больше 0,то рисуем,иначе ничего не делаем
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
            double width = canvas.getWidth();//получаем ширину холста
            double height = canvas.getHeight();//получаем высоту холста
            double diameter = 0.8 * Math.min(width, height);//диаметр выбираем как 0.8 от минимума ширины и высоты
            graphicsContext.setStroke(Color.BLACK);//ставим цвет линии чёрный
            graphicsContext.setLineWidth(1);//ставим толщину линии
            graphicsContext.setFont(new Font("Segoe Print", 12));//ставим шрифт и его размер
            graphicsContext.setFontSmoothingType(FontSmoothingType.LCD);//устанавливаем сглаживание шрифтов
            graphicsContext.strokeRoundRect(width / 2 + 1.1 * diameter / 2, 0.1 * height, 0.8 * (width / 2 - diameter / 2), (colors.size() + 1) * 0.02 * height, 20, 20);//рисуем прямоугольную рамку
            for (int i = 0; i < kdPoints.size(); i++) {//цикл по числу графиков
                graphicsContext.setFill(colors.get(i));//устанавливаем текцщий цвет
                graphicsContext.fillOval(width / 2 + 1.2 * diameter / 2 - 5, 0.1 * height + (i + 1) * 0.02 * height - 5, 10, 10);//рисуем кружочек(образец цвета)
                graphicsContext.strokeText(kdPoints.get(i).getName(), width / 2 + 1.2 * diameter / 2 - 5 + 20, 0.1 * height + (i + 1) * 0.02 * height + 5);//пишем название
            }
        }
    }

    //метод очистки холста
    public void clear() {
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, width, height);//очищаем по всей ширине и высоте холста
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    public void setPoints(List<KDPoint> kdPoints) {
        this.kdPoints = kdPoints;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }
}
