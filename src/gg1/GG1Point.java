package gg1;

//класс точки для гистограммы
public class GG1Point {
    private String series1;
    private String series2;

    public GG1Point(String series1, String series2) {
        this.series1 = series1;
        this.series2 = series2;
    }

    public String getSeries1() {
        return series1;
    }

    public void setSeries1(String series1) {
        this.series1 = series1;
    }

    public String getSeries2() {
        return series2;
    }

    public void setSeries2(String series2) {
        this.series2 = series2;
    }
}
