package gg3;

//класс точки для нормированной гистограммы
public class GG3Point {
    private String value;

    public GG3Point(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
