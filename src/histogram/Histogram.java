package histogram;

import gg1.GG1Point;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;

import java.util.List;
import java.util.Locale;

public class Histogram {
    private List<GG1Point> points;//список точек
    private Canvas canvas;//холст

    public Histogram(List<GG1Point> points, Canvas canvas) {
        this.canvas = canvas;
        this.points = points;
    }

    //метод рисования графиков
    public void drawHistogram() {
        if (!points.isEmpty()) {//если список точек не пуст
            double width = canvas.getWidth();//получаем ширину холста
            double height = canvas.getHeight();//получаем высоту холста
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);//устанавливаем сглаживание шрифтов
            graphicsContext.setFont(new Font("Arial", 12));//устанавливаем шрифт и размер шрифта
            graphicsContext.setLineWidth(1);//устанавливаем толщину линии
            double x0 = 0.05 * width;//центр координат
            double y0 = 0.9 * height;//центр координат
            GG1Point maxPoint = points.stream().max((o1, o2) -> {
                double m1 = Double.max(Double.parseDouble(o1.getSeries1()), Double.parseDouble(o1.getSeries2()));
                double m2 = Double.max(Double.parseDouble(o2.getSeries1()), Double.parseDouble(o2.getSeries2()));
                return Double.compare(m1, m2);
            }).get();//получаем точку с максимальной координатой
            double max = Double.max(Double.parseDouble(maxPoint.getSeries1()), Double.parseDouble(maxPoint.getSeries2()));//максимально значение среди всех точек
            double scale = 0.8 * height / Math.round(max);//масштаб
            double w = 0.95 * width / (3 * points.size() + 1);//ширина столбиков и промежутков между нимим
            double temp = x0 + w;//текущий отступ(первый промежуток)
            int i = 1;
            for (GG1Point gg1Point : points) {//в цикле по всем точкам
                graphicsContext.setFill(Color.RED);//устанавливаем красный цвет для заливки
                graphicsContext.setStroke(Color.RED);//устанавливаем красный цвет для линий
                graphicsContext.fillRect(temp, y0 - 0.05 * height - Double.parseDouble(gg1Point.getSeries1()) * scale, w, Double.parseDouble(gg1Point.getSeries1()) * scale);//заполняем столбик
                graphicsContext.strokeLine(temp, y0 - 0.05 * height - Double.parseDouble(gg1Point.getSeries1()) * scale, temp + w, y0 - 0.05 * height - Double.parseDouble(gg1Point.getSeries1()) * scale);//проводим линию внизу(если знаечние 0,то будет просто линия внизу)
                temp += w;//переходим к следующему столбику
                graphicsContext.setFill(Color.BLACK);//устанавливаем чёрный цвет для заливки
                graphicsContext.fillText(String.valueOf(i), temp, y0 - 0.05 * height + 20);//пигем текущий номер точки
                graphicsContext.setFill(Color.BLUE);//устанавливаем синий цвет для заливки
                graphicsContext.setStroke(Color.BLUE);//устанавливаем синий цвет для линий
                graphicsContext.fillRect(temp, y0 - 0.05 * height - Double.parseDouble(gg1Point.getSeries2()) * scale, w, Double.parseDouble(gg1Point.getSeries2()) * scale);//заполняем столбик
                graphicsContext.strokeLine(temp, y0 - 0.05 * height - Double.parseDouble(gg1Point.getSeries2()) * scale, temp + w, y0 - 0.05 * height - Double.parseDouble(gg1Point.getSeries2()) * scale);//проводим линию внизу(если знаечние 0,то будет просто линия внизу)
                temp += 2 * w;//промежуток + текущий столбик
                i++;
            }
        }
    }

    //метод рисования осей
    public void drawAxes() {
        if (!points.isEmpty()) {//если список точек не пуст
            double width = canvas.getWidth();//получаем ширину холста
            double height = canvas.getHeight();//получаем высоту холста
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
            graphicsContext.setFill(Color.BLACK);//устанавливаем чёрный цвет для заливки
            graphicsContext.setStroke(Color.BLACK);//устанавливаем чёрный цвет для линий
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);//устанавливаем сглаживание шрифтов
            graphicsContext.setLineWidth(0.5);//стави толщину линии
            graphicsContext.setFont(new Font("Arial", 12));//устанавливаем шрифт и размер шрифта
            double x0 = 0.05 * width;//центр координат
            double y0 = 0.9 * height;//центр координат
            GG1Point maxPoint = points.stream().max((o1, o2) -> {
                double m1 = Double.max(Double.parseDouble(o1.getSeries1()), Double.parseDouble(o1.getSeries2()));
                double m2 = Double.max(Double.parseDouble(o2.getSeries1()), Double.parseDouble(o2.getSeries2()));
                return Double.compare(m1, m2);
            }).get();//получаем точку с максимальной координатой
            double max = Double.max(Double.parseDouble(maxPoint.getSeries1()), Double.parseDouble(maxPoint.getSeries2()));
            double scale = 0.8 * height / Math.round(max);//масштаб
            double m = (Math.round(max) / 10d);//цена деления шкалы
            for (int i = 0; i <= 10; i++) {
                graphicsContext.strokeLine(x0, y0 - 0.05 * height - scale * i * m, width, y0 - scale * i * m - 0.05 * height);//проводим горизонтальную линию на уровне цены деления
                graphicsContext.fillText(String.format(Locale.US, "%.1f", i * m), 0, y0 - 0.05 * height - scale * i * m);//подписываем значением
            }
        }
    }

    //метод очистки холста
    public void clear() {
        double width = canvas.getWidth();//получаем ширину холста
        double height = canvas.getHeight();//получаем высоту холста
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, width, height);//очищаем по всей ширине и высоте холста
    }

    //метод рисования легенды
    public void drawLegend() {
        if (!points.isEmpty()) {//если список точек не пуст
            double width = canvas.getWidth();//получаем ширину холста
            double height = canvas.getHeight();//получаем высоту холста
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();//получаем контекст для рисования
            graphicsContext.setFill(Color.BLACK);//устанавливаем чёрный цвет для заливки
            graphicsContext.setStroke(Color.BLACK);//устанавливаем чёрный цвет для линий
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);//устанавливаем сглаживание шрифтов
            graphicsContext.setLineWidth(0.5);//устанавливаем толщину линий
            graphicsContext.setFont(new Font("Arial", 12));//устанавливаем шрифт и размер шрифта
            double x0 = 0.05 * width;//центр координат
            double y0 = 0.9 * height;//центр координат
            graphicsContext.setFill(Color.RED);//устанавливаем крансый цвет для заливки
            graphicsContext.fillRect(x0 + 0.95 * 0.25 * width, y0 + 0.025 * height, 0.025 * height, 0.025 * height);//заполняем образец(квадратик)
            graphicsContext.setFill(Color.BLACK);//устанавливаем чёрный цвет для заливки
            graphicsContext.fillText("Серия 1", x0 + 0.95 * 0.25 * width + 0.05 * height, y0 + 0.045 * height);//подписываем
            graphicsContext.setFill(Color.BLUE);//устанавливаем синий цвет для заливки
            graphicsContext.fillRect(x0 + 0.95 * 0.75 * width, y0 + 0.025 * height, 0.025 * height, 0.025 * height);//заполняем образец(квадратик)
            graphicsContext.setFill(Color.BLACK);//устанавливаем чёрный цвет для заливки
            graphicsContext.fillText("Серия 2", x0 + 0.95 * 0.75 * width + 0.05 * height, y0 + 0.045 * height);//подписываем
        }
    }

    public void setGraphics(List<GG1Point> points) {
        this.points = points;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }
}
