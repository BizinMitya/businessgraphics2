package histogram;

import gg3.GG3Point;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontSmoothingType;

import java.util.List;

public class NormalizedHistogram {
    private List<GG3Point> points;//список точек
    private Canvas canvas;//холст

    public NormalizedHistogram(List<GG3Point> points, Canvas canvas) {
        this.canvas = canvas;
        this.points = points;
    }

    //аналогично обычной гистограмме

    //метод рисования графиков
    public void drawHistogram() {
        if (!points.isEmpty()) {//если список точек не пуст
            double width = canvas.getWidth();
            double height = canvas.getHeight();
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);
            graphicsContext.setFont(new Font("Arial", 12));//устанавливаем шрифт и размер шрифта
            graphicsContext.setLineWidth(1);
            double x0 = 0.05 * width;
            double y0 = 0.9 * height;
            double sum = Double.parseDouble(points.stream().reduce((gg3Point, gg3Point2) -> new GG3Point(String.valueOf(Double.parseDouble(gg3Point.getValue()) + Double.parseDouble(gg3Point2.getValue())))).get().getValue());
            double scale = 0.8 * height / Math.round(sum);
            double w = 0.95 * width / (2 * points.size() + 1);
            double temp = x0 + w;
            int i = 1;
            for (GG3Point gg3Point : points) {
                graphicsContext.setFill(Color.LIGHTGREEN);
                graphicsContext.fillRect(temp, y0 - 0.05 * height - sum * scale, w, sum * scale);
                graphicsContext.setFill(Color.BLACK);
                graphicsContext.fillText(String.valueOf(i), temp + w / 2, y0 - 0.05 * height + 20);
                graphicsContext.setFill(Color.GREEN);
                graphicsContext.setStroke(Color.GREEN);
                graphicsContext.fillRect(temp, y0 - 0.05 * height - Double.parseDouble(gg3Point.getValue()) * scale, w, Double.parseDouble(gg3Point.getValue()) * scale);
                graphicsContext.strokeLine(temp, y0 - 0.05 * height - Double.parseDouble(gg3Point.getValue()) * scale, temp + w, y0 - 0.05 * height - Double.parseDouble(gg3Point.getValue()) * scale);
                temp += 2 * w;
                i++;
            }
        }
    }

    //метод рисования осей
    public void drawAxes() {
        if (!points.isEmpty()) {//если список точек не пуст
            double width = canvas.getWidth();
            double height = canvas.getHeight();
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
            graphicsContext.setFill(Color.BLACK);
            graphicsContext.setStroke(Color.BLACK);
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);
            graphicsContext.setLineWidth(0.5);
            graphicsContext.setFont(new Font("Arial", 12));//устанавливаем шрифт и размер шрифта
            double x0 = 0.05 * width;
            double y0 = 0.9 * height;
            double sum = Double.parseDouble(points.stream().reduce((gg3Point, gg3Point2) -> new GG3Point(String.valueOf(Double.parseDouble(gg3Point.getValue()) + Double.parseDouble(gg3Point2.getValue())))).get().getValue());
            double scale = 0.8 * height / Math.round(sum);
            double m = (Math.round(sum) / 10d);
            for (int i = 0; i <= 10; i++) {
                graphicsContext.strokeLine(x0, y0 - 0.05 * height - scale * i * m, width, y0 - scale * i * m - 0.05 * height);
                graphicsContext.fillText(10 * i + "%", 0, y0 - 0.05 * height - scale * i * m);
            }
        }
    }

    //метод очистки холста
    public void clear() {
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0, 0, width, height);//очищаем по всей ширине и высоте холста
    }

    //метод рисования легенды
    public void drawLegend() {
        if (!points.isEmpty()) {//если список точек не пуст
            double width = canvas.getWidth();
            double height = canvas.getHeight();
            GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
            graphicsContext.setFill(Color.BLACK);
            graphicsContext.setStroke(Color.BLACK);
            graphicsContext.setFontSmoothingType(FontSmoothingType.GRAY);
            graphicsContext.setLineWidth(0.5);
            graphicsContext.setFont(new Font("Arial", 12));//устанавливаем шрифт и размер шрифта
            double x0 = 0.05 * width;
            double y0 = 0.9 * height;
            graphicsContext.setFill(Color.GREEN);
            graphicsContext.fillRect(x0 + 0.95 * 0.5 * width, y0 + 0.025 * height, 0.025 * height, 0.025 * height);
            graphicsContext.setFill(Color.BLACK);
            graphicsContext.fillText("Значения", x0 + 0.95 * 0.5 * width + 0.05 * height, y0 + 0.045 * height);
        }
    }

    public void setGraphics(List<GG3Point> points) {
        this.points = points;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }
}
