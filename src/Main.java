import gg1.GG1Pane;
import gg3.GG3Pane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import kd.KDPane;

public class Main extends Application {

    public static void main(String[] args) {
        Application.launch(Main.class, args);//запуск JavaFX приложения,вызывается метод start(),который ниже
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GG1Pane gg1Pane = new GG1Pane();//создаём панель для обычного графика
        GG3Pane gg3Pane = new GG3Pane();//создаём панель для графика с накоплением
        KDPane kdPane = new KDPane();//создаём панель для круговой диаграммы

        Tab gf1Tab = new Tab("Обычная гистограмма");//создаём вкладку с названием
        gf1Tab.setClosable(false);//устанавливаем свойство незакрываемости вкладки
        gf1Tab.setOnSelectionChanged(event -> {
            gf1Tab.setContent(gg1Pane.getPane());//устанавливаем в качестве содержимого вкладки соответствующую панель
        });

        Tab gf2Tab = new Tab("Нормированная гистограмма");//создаём вкладку с названием
        gf2Tab.setClosable(false);//устанавливаем свойство незакрываемости вкладки
        gf2Tab.setOnSelectionChanged(event -> {
            gf2Tab.setContent(gg3Pane.getPane());//устанавливаем в качестве содержимого вкладки соответствующую панель
        });

        Tab kdTab = new Tab("Круговая диаграмма");//создаём вкладку с названием
        kdTab.setClosable(false);//устанавливаем свойство незакрываемости вкладки
        kdTab.setOnSelectionChanged(event -> {
            kdTab.setContent(kdPane.getPane());//устанавливаем в качестве содержимого вкладки соответствующую панель
        });

        TabPane tabPane = new TabPane(gf1Tab, gf2Tab, kdTab);//создаём панель вкладок и добавляем туда вкладки,которые описали выше

        Scene scene = new Scene(tabPane);//создаём сцену,в качестве корневой панели ставим панель вкладок,задаём размеры сцены
        primaryStage.setScene(scene);//добавляем сцену к окну
        primaryStage.setTitle("Деловая графика");//устанавливаем название окна
        primaryStage.setMaximized(true);//раскрываем окно во весь экран
        primaryStage.show();//показываем окно
    }
}
